package iot.prediction.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

//@EnableSwagger2
@SpringBootApplication
public class PredictionApi {
	
	public static void main(String[] args) {
		SpringApplication.run(PredictionApi.class, args);
	}

}
