package iot.prediction.api.domain;

public class Prediction {

	int predictionDays;
	float threshSholdTemperature;
	int energyConsumption;
	
	
	public Prediction() {
		// TODO Auto-generated constructor stub
	}
	public Prediction(int predictionDays, float threshSholdTemperature,
			int energyConsumption) {
		super();
		this.predictionDays = predictionDays;
		this.threshSholdTemperature = threshSholdTemperature;
		this.energyConsumption = energyConsumption;
	}
	public int getPredictionDays() {
		return predictionDays;
	}
	public void setPredictionDays(int predictionDays) {
		this.predictionDays = predictionDays;
	}
	public float getThreshSholdTemperature() {
		return threshSholdTemperature;
	}
	public void setThreshSholdTemperature(float threshSholdTemperature) {
		this.threshSholdTemperature = threshSholdTemperature;
	}
	public int getEnergyConsumption() {
		return energyConsumption;
	}
	public void setEnergyConsumption(int energyConsumption) {
		this.energyConsumption = energyConsumption;
	}
	@Override
	public String toString() {
		return "Prediction [predictionDays=" + predictionDays
				+ ", threshSholdTemperature=" + threshSholdTemperature
				+ ", energyConsumption=" + energyConsumption + "]";
	}
	
	
	
	
	
}
