package iot.prediction.api.controller;

import iot.prediction.api.domain.Prediction;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.HConnection;
import org.apache.hadoop.hbase.client.HConnectionManager;
import org.apache.hadoop.hbase.client.HTableInterface;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.util.Bytes;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class PredictionController {
	
	
	private static final String PREDICTION_TABLE_NAME = "prediction_analysis";
	
	private HConnection connection;
	  private HTableInterface predictionAnalysis;
	  
	  List<Prediction> predictionData=null;
	  public static Configuration constructConfiguration() {
		Configuration config = HBaseConfiguration.create();

		config.set("hbase.zookeeper.property.clientPort", "2181");
		config.set("hbase.zookeeper.quorum", "sandbox-hdp.hortonworks.com");
		config.set("zookeeper.znode.parent", "/hbase-unsecure");

		//LOG.info("config" + config.toString());
		return config;
	}

	public ResponseEntity<List> test(){
		try {
			
			 predictionData=new ArrayList<Prediction>();
			
			this.connection = HConnectionManager
					.createConnection(constructConfiguration());
			this.predictionAnalysis = connection.getTable(PREDICTION_TABLE_NAME);
			Scan scan=new Scan();
			ResultScanner sc = this.predictionAnalysis.getScanner(scan);
			for (Result rr = sc.next(); rr != null; rr = sc.next()) {
				float threshold = Float.valueOf(Bytes.toString(rr.getValue(
						Bytes.toBytes("analysis"), Bytes.toBytes("Thresholdtemperature"))));
			
				int energy = Integer.valueOf(Bytes.toString(rr.getValue(
						Bytes.toBytes("analysis"), Bytes.toBytes("EnergyPrediction"))));
				int days = Integer.valueOf(Bytes.toString(rr.getValue(
						Bytes.toBytes("analysis"), Bytes.toBytes("PredictionDays"))));
				
				predictionData.add(new Prediction(days, threshold, energy));
			}
			return new ResponseEntity<List>(predictionData,HttpStatus.OK);
		} catch (IOException e) {
		
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		
		
	}
	
	
	@RequestMapping(value="/getData",method=RequestMethod.GET,produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getData(){
		return test();
	}
	
	
}
